# Bank

O projeto Bank é um projeto simples em Java de simulação de um banco. Foi desenvolvido para fins educacionais, para aprendermos sobre versionamento com Git e repositórios remotos.

## Funcionalidades

- Ver extrato da conta
- Depósito em conta
- Saque da conta
- Transferência em Pix
- Pagamento
- Investimento

## Instalando

Deverá ser executado o arquivo JAVA do projeto a partir de uma IDE.

### Pré-requisitos

- Java Development Kit
- Um ambiente de desenvolvimento integrado (IDE) que suporte Maven, de preferência o Apache NetBeans

### Instalando
1. Na página do repositório, clique no botão Code.
2. Clique no botão Download ZIP.
3. Salve o arquivo zipado no seu computador.
4. Descompacte o arquivo zipado.
5. Abra o projeto na sua IDE de escolha.
6. Execute o código.

## Autores
- Jacqueline Limas
- Lais Barbizan
- Milena Bregalda
- Rodrigo Fontoura
